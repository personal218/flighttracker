package FlightTracker.Application;

import FlightTracker.DataAccessLayer.BaseDataAccess;
import FlightTracker.DataAccessLayer.MySQLDataAccess;
import FlightTracker.OpenSkyNetwork.Flight;
import FlightTracker.OpenSkyNetwork.RequestAPI.*;
import FlightTracker.OpenSkyNetwork.State;
import FlightTracker.Utilities.TimeUtilities;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static FlightTracker.OpenSkyNetwork.Utilities.ParseStatesJSON;

public class Application {

    private static Map<String, String>params = new HashMap<>();


    public static void main (String[] args) throws IOException, InterruptedException {
        System.out.println("FlightTracker: FlightTracker.Application.main\n");

        ParseParams(args);
        String userName = getParamValue(Constants.ARGUMENT_USERNAME);
        String password = getParamValue(Constants.ARGUMENT_PASSWORD);

        LocalDateTime end = LocalDateTime.now().truncatedTo(ChronoUnit.DAYS);
        if (paramExists(Constants.ARGUMENT_END)){
            end = LocalDateTime.parse(getParamValue(Constants.ARGUMENT_END));
        }

        LocalDateTime start = end.minusDays(29);
        if (paramExists(Constants.ARGUMENT_START)){
            start = LocalDateTime.parse(getParamValue(Constants.ARGUMENT_START));
        }

        if (paramExists(Constants.ARGUMENT_LOADARRIVALS)){
            List<String> arrivalAirports = Arrays.asList(getParamValue(Constants.ARGUMENT_LOADARRIVALS).split(","));
            //TODO: Make this a bit more elegant, so don't have to pass airport, start, end twice... Possibly put getter
            LoadFlights(new AirportArrivalsRequest(arrivalAirports.get(0), start, end), arrivalAirports, start, end,1);
        }

        if (paramExists(Constants.ARGUMENT_LOADDEPARTURES)){
            List<String> departureAirports = Arrays.asList(getParamValue(Constants.ARGUMENT_LOADDEPARTURES).split(","));
            //TODO: Make this a bit more elegant, so don't have to pass airport, start, end twice... Possibly put getter
            LoadFlights(new AirportDeparturesRequest(departureAirports.get(0), start, end), departureAirports, start, end, 1);
        }
        if (paramExists(Constants.ARGUMENT_LOADICAO24)){
            List<String> icao24List = Arrays.asList(getParamValue(Constants.ARGUMENT_LOADICAO24).split(","));
            //TODO: Make this a bit more elegant, so don't have to pass ica24, start, end twice... Possibly put getter
            LoadFlights(new AircraftFlightRequest(icao24List.get(0), start, end), icao24List, start, end, 1);
        }

//        ParseStatesJSON("");
        // get flights for N80847 from May 1, 2020 00:00 PDT - May 13, 2020 00:00 PDT
//        end = LocalDateTime.now().truncatedTo(ChronoUnit.DAYS);
//        LocalDateTime begin = end.minusDays(29);
//
//        AircraftFlightRequest flights = new AircraftFlightRequest(Constants.N80847, begin, end );
//        flights.Execute();
//
//        System.out.println("Flights for N80847: " + begin + " -> " + end);
//        flights.OutputFlights();
//        List<Flight> flightList = flights.getFlights();


        // Get Arrivals for KRNT
//        AirportArrivalsRequest arrivalsRequest = new AirportArrivalsRequest("KRNT",                  // Airport
//                                                            end.minusDays(7),       // Begin
//                                                            end);                  // End
//        arrivalsRequest.Execute();
//
//        System.out.print("\nArrivals to KRNT from " + end.minusDays(7) + " -> " + end +":\n");
//        arrivalsRequest.OutputFlights();

        // Get Flights from 2020-09-25T12:00:00 to 2020-09-25T13:00:00
//        TimeIntervalFlightsRequest timeIntervalFlightsRequest = new TimeIntervalFlightsRequest(LocalDateTime.parse("2020-09-25T12:00:00"),
//                                                                                        LocalDateTime.parse("2020-09-25T13:00:00"));
//        timeIntervalFlightsRequest.Execute();
//        System.out.println("\nFlights during interval:");
//        timeIntervalFlightsRequest.OutputFlights();

//        System.out.println("\nState Vectors Request at: " + LocalDateTime.now());
//        StateVectorsRequest stateVectorsRequest = new StateVectorsRequest(LocalDateTime.now(), 49.0, -122.0, 46.0, -125.0);
//        stateVectorsRequest.Execute();
//        System.out.println(stateVectorsRequest.getResponseContent());
//        List<State> states = stateVectorsRequest.getStateVectors();
//        stateVectorsRequest.OutputStateVectors();

//        Flight flight = flightList.get(0);
//        LocalDateTime flightStart = FlightTracker.Utilities.TimeUtilities.EpochSecondsToDate(flight.firstSeen);
//        System.out.println("FlightTrack for N80847 " + flightStart);
//        TrackRequest flightTrackRequest = new TrackRequest(Constants.N80847, flightStart, userName, password);
//        flightTrackRequest.Execute();
//        System.out.println(flightTrackRequest.getResponseContent());
//        flightTrackRequest.getTracks();
    }

    private static void ParseParams(String[] args) {
        System.out.println("Args:");
        for (String arg : args) {
            String argSplit[] = arg.split(":", 2);

            if (argSplit.length > 1) {
                params.put(argSplit[0], argSplit[1]);
            }
            else{
                params.put(arg, "");
            }
        }
        params.forEach((param, value) -> System.out.println (("\t" + param + ":\t" + value)));
    }

    private static String getParamValue (String paramName){
        String returnValue = "";

        if (params.containsKey(paramName)) returnValue = params.get(paramName);

        return returnValue;
    }

    private static boolean paramExists (String paramName){
        return (params.containsKey(paramName));
    }

    private static void LoadFlights( FlightsRequest flightsRequest, List<String> ids, LocalDateTime begin, LocalDateTime end, long interval) throws IOException, InterruptedException {
        //TODO: This needs to moved someplace else... i.e. member of of main application
        BaseDataAccess dbAccess = new MySQLDataAccess("localhost", "test_db", "javauser", "Skyh@wk172");

        for(String id : ids) {
            LocalDateTime stop = end;
            LocalDateTime start = stop.minusDays(interval);

            while (stop.isAfter(begin)) {
                System.out.println("Getting " + id + " " + flightsRequest.getClass().getSimpleName() +" " + start + " --> " + stop);
                flightsRequest.UpdateParameters(id,
                                                TimeUtilities.LocalDateTimeToEpochSeconds(start),
                                                TimeUtilities.LocalDateTimeToEpochSeconds(stop));
                int responseCode = flightsRequest.Execute();
                if (responseCode != 200 && responseCode != 404){
                    int retryCount = 0;
                    while (retryCount < 10 && responseCode != 200){
                        System.out.println("Request returned error: " + responseCode + " - " + flightsRequest.getResponseMessage());
                        System.out.println("Sleeping...");
                        Thread.sleep(30000);
                        System.out.println("Retrying...");
                        responseCode = flightsRequest.Execute();
                        retryCount++;
                    }
                    if (responseCode == 200){
                        System.out.println("Succeeded!");
                    }
                    else{
                        System.out.println("Exceeded retry count!");
                    }
                }
                if (responseCode == 200) {
                    System.out.println("Inserting flights to DB...");
                    dbAccess.InsertFlights(flightsRequest.getFlights());
                    System.out.println("flights inserted");
                }
                stop = stop.minusDays(interval);
                start = start.minusDays(interval);
            }
        }

    }
}