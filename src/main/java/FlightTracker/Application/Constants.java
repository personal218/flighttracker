package FlightTracker.Application;

public class  Constants {
    public static final String N80847 = "ab0299";
    public static final String N1538V = "a0d8d8";
    public static final String N5996J = "a7c209";

    public static final String ARGUMENT_USERNAME = "/username";
    public static final String ARGUMENT_PASSWORD = "/password";

    public static final String ARGUMENT_LOADARRIVALS = "/loadarrivals";
    public static final String ARGUMENT_LOADDEPARTURES = "/loaddepartures";
    public static final String ARGUMENT_LOADICAO24 = "/loadicao24";

    public static final String ARGUMENT_START = "/start";
    public static final String ARGUMENT_END = "/end";
}
