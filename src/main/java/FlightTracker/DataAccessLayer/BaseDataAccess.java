package FlightTracker.DataAccessLayer;


import FlightTracker.OpenSkyNetwork.Flight;

import java.util.List;

public abstract class BaseDataAccess {
    public abstract void InsertFlight(Flight flight);
    public abstract void DeleteFlight(Flight flight);
    public abstract void QueryFlight(Flight flight);

    public abstract void InsertFlights(List<Flight> flightList);
}
