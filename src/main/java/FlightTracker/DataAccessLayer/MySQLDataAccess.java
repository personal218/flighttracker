package FlightTracker.DataAccessLayer;

import FlightTracker.OpenSkyNetwork.Flight;
import FlightTracker.Utilities.TimeUtilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.List;

public class MySQLDataAccess extends BaseDataAccess {
    private String dbUrl = "";
    private Connection connection;
    private String flightsTable = "tbl_flights";
    String user;
    String password;

    //TODO: This should be constant static
    private String flightsInsertStatement = "INSERT IGNORE INTO " + flightsTable + " ("
                                                + "icao24, "
                                                + "firstSeen, "
                                                + "estDepartureAirport, "
                                                + "lastSeen, "
                                                + "estArrivalAirport, "
                                                + "callsign, "
                                                + "estDepartureAirportHorizDistance, "
                                                + "estDepartureAirportVertDistance, "
                                                + "estArrivalAirportHorizDistance, "
                                                + "estArrivalAirportVertDistance, "
                                                + "departureAirportCandidatesCount, "
                                                + "arrivalAirportCandidatesCount,"
                                                + "firstSeenDateTime, "
                                                + "lastSeenDateTime, "
                                                + "flightid)"
                                            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";


    public MySQLDataAccess(String host, String database, String user, String password){
        this.dbUrl ="jdbc:mysql://" + host + "/" + database;
        this.user = user;
        this.password = password;
    }

    private void SetFlightInsertPreparedItem(PreparedStatement preparedStatement, Flight flight){
        try{
            preparedStatement.setString(1, flight.icao24 == null ? flight.icao24 : flight.icao24.trim());
            preparedStatement.setLong(2, flight.firstSeen);
            preparedStatement.setString(3, flight.estDepartureAirport == null ? flight.estDepartureAirport : flight.estDepartureAirport.trim());
            preparedStatement.setLong(4, flight.lastSeen);
            preparedStatement.setString(5, flight.estArrivalAirport == null ? flight.estArrivalAirport : flight.estArrivalAirport.trim());
            preparedStatement.setString(6, flight.callsign == null ? flight.callsign : flight.callsign.trim());
            preparedStatement.setLong(7, flight.estDepartureAirportHorizDistance);
            preparedStatement.setLong(8, flight.estDepartureAirportVertDistance);
            preparedStatement.setLong(9, flight.estArrivalAirportHorizDistance);
            preparedStatement.setLong(10, flight.estArrivalAirportVertDistance);
            preparedStatement.setInt(11, flight.departureAirportCandidatesCount);
            preparedStatement.setInt(12, flight.arrivalAirportCandidatesCount);
            preparedStatement.setString(13, TimeUtilities.EpochSecondsToDate(flight.firstSeen).toString().trim());
            preparedStatement.setString(14, TimeUtilities.EpochSecondsToDate(flight.lastSeen).toString().trim());
            preparedStatement.setString(15, flight.icao24 + ":" + Long.toString(flight.firstSeen) + ":" + Long.toString(flight.lastSeen));
        }
        catch (Exception exception){
            System.out.println("Caught Exception: " + exception.toString());
        }

    }

    public void InsertFlight(Flight flight){
        try{
            this.connection = DriverManager.getConnection(this.dbUrl, this.user, this.password);
            PreparedStatement preparedStatement = this.connection.prepareStatement(this.flightsInsertStatement);

            SetFlightInsertPreparedItem(preparedStatement, flight);

            preparedStatement.execute();
            this.connection.close();
        }
        catch (Exception exception){
            System.out.print("Caught Exception: " + exception.toString());
        }

    }

    public void InsertFlights(List<Flight> flightList){
        try{
            this.connection = DriverManager.getConnection(this.dbUrl, this.user, this.password);
            PreparedStatement preparedStatement = this.connection.prepareStatement(this.flightsInsertStatement);

            for(Flight flight : flightList){
                this.SetFlightInsertPreparedItem(preparedStatement, flight);
                preparedStatement.executeUpdate();
            }

            this.connection.close();
        }
        catch(Exception exception){
            System.out.println("Caught Exception: " + exception.toString());

        }
    }

    public void DeleteFlight(Flight flight){
        // Not Implemented
        throw new UnsupportedOperationException("Not Implemented at this time");

    }

    public void QueryFlight(Flight flight){
        // Not Implemented
        throw new UnsupportedOperationException("Not Implemented at this time");
    }
}
