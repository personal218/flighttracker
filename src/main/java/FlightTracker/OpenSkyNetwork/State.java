package FlightTracker.OpenSkyNetwork;

public class State {
    /*
0	icao24	        string	Unique ICAO 24-bit address of the transponder in hex string representation.
1	callsign	    string	Callsign of the vehicle (8 chars). Can be null if no callsign has been received.
2	origin_country	string	Country name inferred from the ICAO 24-bit address.
3	time_position	int	    Unix timestamp (seconds) for the last position update. Can be null if no position report was received by OpenSky within the past 15s.
4	last_contact	int	    Unix timestamp (seconds) for the last update in general. This field is updated for any new, valid message received from the transponder.
5	longitude	    float	WGS-84 longitude in decimal degrees. Can be null.
6	latitude	    float	WGS-84 latitude in decimal degrees. Can be null.
7	baro_altitude	float	Barometric altitude in meters. Can be null.
8	on_ground	    boolean	Boolean value which indicates if the position was retrieved from a surface position report.
9	velocity	    float	Velocity over ground in m/s. Can be null.
10	true_track	    float	True track in decimal degrees clockwise from north (north=0°). Can be null.
11	vertical_rate	float	Vertical rate in m/s. A positive value indicates that the airplane is climbing, a negative value indicates that it descends. Can be null.
12	sensors	        int[]	IDs of the receivers which contributed to this state vector. Is null if no filtering for sensor was used in the request.
13	geo_altitude	float	Geometric altitude in meters. Can be null.
14	squawk	        string	The transponder code aka Squawk. Can be null.
15	spi	            boolean	Whether flight status indicates special purpose indicator.
16	position_source	int     Origin of this state’s position: 0 = ADS-B, 1 = ASTERIX, 2 = MLAT
     */

    public String icao24;           // Unique ICAO 24-bit address of the transponder in hex string representation.
    public String callsign;         // Callsign of the vehicle (8 chars). Can be null if no callsign has been received.
    public String originCountry;    // Country name inferred from the ICAO 24-bit address.
    public Integer timePosition;
    public Integer lastContact;
    public Double longitude;
    public Double latitude;
    public Double baroAltitude;
    public Boolean onGround;
    public Double velocity;
    public Double trueTrack;
    public Double verticalRate;
    public Integer[] sensors;
    public Double geoAltitude;
    public String squawk;
    public Boolean spi;
    public Integer positionSource;

    public State (){}
    public State(String icao24,
                 String callsign,
                 String originCountry,
                 Integer timePosition,
                 Integer lastContact,
                 Double longitude,
                 Double latitude,
                 Double baroAltitude,
                 Boolean onGround,
                 Double velocity,
                 Double trueTrack,
                 Double verticalRate,
                 Integer[] sensors,
                 Double geoAltitude,
                 String squawk,
                 Boolean spi,
                 Integer positionSource){
        this.icao24 = icao24;
        this.callsign = callsign;
        this.originCountry = originCountry;
        this.timePosition = timePosition;
        this.lastContact = lastContact;
        this.longitude = longitude;
        this.latitude = latitude;
        this.baroAltitude = baroAltitude;
        this.onGround = onGround;
        this.velocity = velocity;
        this.trueTrack = trueTrack;
        this.verticalRate = verticalRate;
        this.sensors = sensors;
        this.geoAltitude = geoAltitude;
        this.squawk = squawk;
        this.spi = spi;
        this.positionSource = positionSource;

    }
}
