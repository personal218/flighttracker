package FlightTracker.OpenSkyNetwork.RequestAPI;

import FlightTracker.OpenSkyNetwork.StateVectors;
import FlightTracker.Utilities.Parameter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalDateTime;
import java.util.List;

import static FlightTracker.Utilities.TimeUtilities.LocalDateTimeToEpochSeconds;

/**
 * Class to support requesting track information from OpenSkyNetwork for specified aircraft.  Actual execution of
 * the request is implemented in the BaseRequest class
 *
 * @author Wade Hasbrouck - wadehas@gmail.com
 * @version 1.0
 * @since   1.0
 *
 * @see FlightTracker.OpenSkyNetwork.RequestAPI.BaseRequest
 * @see <a href="https://opensky-network.org/apidoc/rest.html#track-by-aircraft">OpenSkyNetwork Track by Aircraft</a>
 */
public class TrackRequest extends BaseRequest{

    /**
     * Constructor for TrackRequest for specific aircraft with OpenSkyNetwork user credentials and time as epoch seconds
     *
     * <p><b>NOTE:</b> Specifying user credentials does not have any added benefit</p>
     *
     * @param icao24    ICAO24 address of the transponder of the aircraft.  This uniquely identifies the aircraft
     * @param time      Time in epoch seconds.  Can be anytime between start and end of a known flight.  If set to 0, will return live track if any flight is ongoing for the aircraft
     * @param userName  OpenSkyNetwork userName for the request.  For anonymous user set to empty string.
     * @param password  password for the specified user.  For anonymous user set to empty string.
     */
    public TrackRequest(String icao24, long time, String userName, String password){
        super (Constants.TracksOperation, userName, password);
        this.parameters.add(new Parameter(Constants.Icao24ParamName, icao24));
        this.parameters.add(new Parameter(Constants.TimeParamName, Long.toString(time)));
    }

    /**
     * Constructor for TrackRequest for specific aircraft as anonymous user and time in epoch seconds
     *
     * <p><b>NOTE:</b> Specifying user credentials does not have any added benefit</p>
     *
     * @param icao24    ICAO24 address of the transponder of the aircraft.  This uniquely identifies the aircraft
     * @param time      Time in epoch seconds.  Can be anytime between start and end of a known flight.  If set to 0, will return live track if any flight is ongoing for the aircraft
     */
    public TrackRequest(String icao24, long time){
        this(icao24, time, "", "");
    }

    /**
     * Constructor for TrackRequest for specific aircraft with OpenSkyNetwork user credentials and time LocalDateTime
     *
     * <p><b>NOTE:</b> Specifying user credentials does not have any added benefit</p>
     *
     * @param icao24    ICAO24 address of the transponder of the aircraft.  This uniquely identifies the aircraft
     * @param time      Time as LocalDateTime.  Can be anytime between start and end of a known flight.
     * @param userName  OpenSkyNetwork userName for the request.  For anonymous user, set to empty string
     * @param password  password for the specified user.  For anonymous user, set to empty string
     */
    public TrackRequest(String icao24, LocalDateTime time, String userName, String password){
        this (icao24, LocalDateTimeToEpochSeconds(time), userName, password);
    }
    /**
     * Constructor for TrackRequest for specific aircraft as anonymous user and time as LocalDateTime
     *
     * <p><b>NOTE:</b> Specifying user credentials does not have any added benefit</p>
     *
     * @param icao24    ICAO24 address of the transponder of the aircraft.  This uniquely identifies the aircraft
     * @param time      Time in epoch seconds.  Can be anytime between start and end of a known flight.
     */
    public TrackRequest(String icao24, LocalDateTime time){
        this(icao24, time, "", "");
    }

    /**
     * Methord to return the tracks that were returned in the request to OpenSkyNetwork
     *
     * <p><b>NOTE: THIS FUNCTION SEEMS TO BE INCOMPLETE AND NEEDS TO BE FINISHED!</b>  Reason for this is that
     * this was most likely written as a simple function to test getting and parsing the track information.</p>
     *
     *</p>
     *
     * @throws JsonProcessingException
     */
    public void getTracks() throws JsonProcessingException {
        //TODO: This method seems to be incomplete!
        FlightTrack flightTrack;

        ObjectMapper jsonMapper = new ObjectMapper();
        flightTrack = jsonMapper.readValue(this.getResponseContent(), new TypeReference<FlightTrack>(){});
        System.out.println("Exiting");
    }
}
