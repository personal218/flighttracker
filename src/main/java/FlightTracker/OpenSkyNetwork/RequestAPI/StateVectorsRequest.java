package FlightTracker.OpenSkyNetwork.RequestAPI;

import FlightTracker.OpenSkyNetwork.State;
import FlightTracker.OpenSkyNetwork.StateVectors;
import FlightTracker.Utilities.Parameter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import static FlightTracker.Utilities.TimeUtilities.EpochSecondsToDate;
import static FlightTracker.Utilities.TimeUtilities.LocalDateTimeToEpochSeconds;

/**
 * Class to request for State Vectors from OpenSkyNetwork.
 * <p>
 * This request will get current state vectors for all aircraft that satisfy the query.
 * </p>
 *
 * @author Wade Hasbrouck - <a href="mailto:wadehas@gmail.com">Send mail</a>
 * @version 1.0
 * @since 1.0
 * @see BaseRequest
 * @see <a href="https://opensky-network.org/apidoc/rest.html#all-state-vectors">Open Sky Network</a>
 */
public class StateVectorsRequest extends BaseRequest{

    // TODO: Consider changing this to an enum...  Also don't think these need to be public...
    public static final int StateVectorIndexIcao24          = 0;
    public static final int StateVectorIndexCallsign        = 1;
    public static final int StateVectorIndexOriginCountry   = 2;
    public static final int StateVectorIndexTimePosition    = 3;
    public static final int StateVectorIndexLastContact     = 4;
    public static final int StateVectorIndexLongitude       = 5;
    public static final int StateVectorIndexLatitude        = 6;
    public static final int StateVectorIndexBaroAltitude    = 7;
    public static final int StateVectorIndexOnGround        = 8;
    public static final int StateVectorIndexVelocity        = 9;
    public static final int StateVectorIndexTrueTrack       = 10;
    public static final int StateVectorIndexVerticalRate    = 11;
    public static final int StateVectorIndexSensors         = 12;
    public static final int StateVectorIndexGeoAltitude     = 13;
    public static final int StateVectorIndexSquawk          = 14;
    public static final int StateVectorIndexSpi             = 15;
    public static final int StateVectorIndexPositionSource  = 16;

    /**
     * This was some test data I was using to test JSON parsing...  This should be removed
     */
    public static String testStatesData = "{\"time\":1601245500,\"states\":[[\"a3e911\",\"FFT418  \",\"United States\",1601245499,1601245499,-104.1415,39.7222,4785.36,false,205.6,114.38,12.03,null,4884.42,null,false,0],[\"040190\",\"BOE106  \",\"Ethiopia\",1601245500,1601245500,-123.069,47.8422,5951.22,false,192.29,278.15,12.03,null,6286.5,\"4673\",false,0]]}";

    // TODO: This should probably be private!
    public List<State> stateVectors = new LinkedList<>();

    /**
     * Constructor for State Vector Request with a specified time in epoch seconds and OpenSkyNetwork Credentials.
     * This request will get state vectors for all aircraft in flight.  With OpenSkyNetwork credentials the time must be with the
     * last hour (time lessthan now - 3600s).  If no credentials are supplied, time is ignored.
     *
     * @param time      time to get state vectors for.  See limitations
     * @param username  OpenSkyNetwork username to use for request.  See Limitations
     * @param password  Password for specified username
     */
    public StateVectorsRequest(long time, String username, String password){
        super(Constants.StateVectorsAllOperation, username, password);
        this.parameters.add(new Parameter(Constants.TimeParamName, Long.toString(time)));
    }

    /**
     * Constructor for State Vector Request with a specified time as LocalDateTime and OpenSkyNetwork Credentials.
     * This request will get state vectors for all aircraft in flight.  With OpenSkyNetwork credentials the time must
     * be with in the last hour (time lessthan now - 3600s).  If no credentials are supplied, the time is ignored.
     *
     * @param time      time to get state vectors for.  See Limitations
     * @param userName  OpenSkyNetwork username to use for request.  See limitations
     * @param password  Password for the specified userName
     */
    public StateVectorsRequest(LocalDateTime time, String userName, String password){
        this(LocalDateTimeToEpochSeconds(time), userName, password);
    }

    /**
     * Constructor for State Vector Request as anonymous user and time in epoch seconds.  This request will get
     * State Vectors for all aircraft that are in flight.
     *
     * <p><b>NOTE: </b>
     * Not sure if this constructor really makes sense as without user credentials, time is ignored by OpenSkyNetwork
     * </p>
     *
     * @param time  time as epoch seconds to get state vectors for
     */
    public StateVectorsRequest(long time){
        this(time, "", "");
    }
    /**
     * Constructor for State Vector Request as anonymous user and time in LocalDateTime.  This request will get
     * State Vectors for all aircraft that are in flight.
     *
     * <p><b>NOTE: </b>
     * Not sure if this constructor really makes sense as without user credentials, time is ignored by OpenSkyNetwork
     * </p>
     *
     * @param time  time to get state vectors for
     */
    public StateVectorsRequest(LocalDateTime time){
        this(time, "", "");
    }

    /**
     * Constructor for State Vector request with time as epoch seconds, OpenSkyNetwork Credentials and bounding
     * rectangle defined in latitude and longitude.
     *
     * @param time          time to request state vectors for.  If anonymous user, this is ignored.
     * @param maxLatitude   Maximum latitude of the bounding rectangle
     * @param maxLongitude  Maximum longitude of the bounding rectangle
     * @param minLatitude   Minimum latitude of the bounding rectangle
     * @param minLongitude  Minimum longitude of the bounding rectangle
     * @param username      OpenSkyNetwork username to use for request
     * @param password      password for specified user
     */
    public StateVectorsRequest(long time,
                               Double maxLatitude,
                               Double maxLongitude,
                               Double minLatitude,
                               Double minLongitude,
                               String username,
                               String password){
        this(time, username, password);
        this.parameters.add(new Parameter(Constants.MaxLatitudeParamName, maxLatitude.toString()));
        this.parameters.add(new Parameter(Constants.MaxLongitudeParamName, maxLongitude.toString()));
        this.parameters.add(new Parameter(Constants.MinLatitudeParamName, minLatitude.toString()));
        this.parameters.add(new Parameter(Constants.MinLongitudeParamName, minLongitude.toString()));
    }

    /**
     * Constructor for State Vector request with time as LocalDateTime, OpenSkyNetwork Credentials and bounding
     * rectangle defined in latitude and longitude.
     *
     * @param time          time to request state vectors for.  If anonymous user, this is ignored.
     * @param maxLatitude   Maximum latitude of the bounding rectangle
     * @param maxLongitude  Maximum longitude of the bounding rectangle
     * @param minLatitude   Minimum latitude of the bounding rectangle
     * @param minLongitude  Minimum longitude of the bounding rectangle
     * @param username      OpenSkyNetwork username to use for request
     * @param password      password for specified user
     */
    public StateVectorsRequest(LocalDateTime time,
                               Double maxLatitude,
                               Double maxLongitude,
                               Double minLatitude,
                               Double minLongitude,
                               String username,
                               String password){
        this(LocalDateTimeToEpochSeconds(time), maxLatitude, maxLongitude, minLatitude, minLongitude, username, password);
    }

    /**
     * Method to return a list of StateVectors that were parsed from the JSON contained in the responseContent returned
     * by the request.
     *
     * @return List of StateVectors that were parsed from responseContent returned from request
     * @throws JsonProcessingException Occurs if there is an issue parsing the JSON from responseContent
     */
    //TODO: This can use a little simplification, and possibly move the "mapping" part to a separate method
    public List<State> getStateVectors() throws JsonProcessingException {
        ObjectMapper jsonMapper = new ObjectMapper();
        StateVectors vectors = jsonMapper.readValue(this.getResponseContent(), new TypeReference<StateVectors>(){});
        for (Vector vector : vectors.states) {
            State newStateVector = new State();
            for (int index = 0; index < 17; index++) {
                Object element = vector.elementAt(index);
                if (element != null) {
                    Double value;
                    switch (index) {
                        case StateVectorIndexIcao24:
                            newStateVector.icao24 = (String) element;
                            break;
                        case StateVectorIndexCallsign:
                            newStateVector.callsign = (String) element;
                            break;
                        case StateVectorIndexOriginCountry:
                            newStateVector.originCountry = (String) element;
                            break;
                        case StateVectorIndexTimePosition:
                            newStateVector.timePosition = (Integer)element;
                            break;
                        case StateVectorIndexLastContact:
                            newStateVector.lastContact = (Integer) element;
                            break;
                        case StateVectorIndexLongitude: //TODO: This (and others like it) can be simplified
                            if (element.getClass() == Integer.class){
                                Integer intValue = (Integer)element;
                                value = intValue.doubleValue();
                            }
                            else value = (Double)element;
                            newStateVector.longitude = value;
                            break;
                        case StateVectorIndexLatitude:
                            if (element.getClass() == Integer.class){
                                Integer intValue = (Integer)element;
                                value = intValue.doubleValue();
                            }
                            else value = (Double)element;
                            newStateVector.latitude = value;
                            break;
                        case StateVectorIndexBaroAltitude:
                            if (element.getClass() == Integer.class){
                                Integer intValue = (Integer)element;
                                value = intValue.doubleValue();
                            }
                            else value = (Double)element;
                            newStateVector.baroAltitude = value;
                            break;
                        case StateVectorIndexOnGround:
                            newStateVector.onGround = (Boolean)element;
                            break;
                        case StateVectorIndexVelocity:
                            if (element.getClass() == Integer.class){
                                Integer intValue = (Integer)element;
                                value = intValue.doubleValue();
                            }
                            else value = (Double)element;
                            newStateVector.velocity = value;
                            break;
                        case StateVectorIndexTrueTrack:
                            if (element.getClass() == Integer.class){
                                Integer intValue = (Integer)element;
                                value = intValue.doubleValue();
                            }
                            else value = (Double)element;
                            newStateVector.trueTrack = value;
                            break;
                        case StateVectorIndexVerticalRate:
                            if (element.getClass() == Integer.class){
                                Integer intValue = (Integer)element;
                                value = intValue.doubleValue();
                            }
                            else value = (Double)element;
                            newStateVector.verticalRate = value;
                            break;
                        case StateVectorIndexSensors:
                            break;
                        case StateVectorIndexGeoAltitude:
                            if (element.getClass() == Integer.class){
                                Integer intValue = (Integer)element;
                                value = intValue.doubleValue();
                            }
                            else value = (Double)element;
                            newStateVector.geoAltitude = value;
                            break;
                        case StateVectorIndexSquawk:
                            newStateVector.squawk = (String)element;
                            break;
                        case StateVectorIndexSpi:
                            newStateVector.spi = (Boolean)element;
                            break;
                        case StateVectorIndexPositionSource:
                            newStateVector.positionSource = (Integer)element;
                            break;
                    }
                }
            }
            this.stateVectors.add(newStateVector);
        }
        return this.stateVectors;
    }

    /**
     * Output the list of StateVectors.
     */
    public void OutputStateVectors(){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        for (State vector : this.stateVectors){
            try {
                System.out.printf("%-6s\t%-10s%s\t%3.3f\t%3.3f\t%03.0f\t%3.0f\t%5.0f\t%6.0f\t%6s\n",
                        vector.icao24,
                        vector.callsign,
                        EpochSecondsToDate(vector.timePosition).format(dateTimeFormatter),
                        vector.longitude,
                        vector.latitude,
                        vector.trueTrack,
                        vector.velocity * 1.94384,              // convert from m/s to knots
                        vector.verticalRate * 3.28084 * 100,    // convert from m/s to ft/s
                        vector.geoAltitude * 3.28084,           // convert from meters to ft
                        vector.squawk
                );
            }
            // TODO: Should probably do something here....   Not quite sure what exception we could get here :-)
            catch(Exception exception){}
        }

    }
}
