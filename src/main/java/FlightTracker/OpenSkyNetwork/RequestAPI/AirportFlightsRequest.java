package FlightTracker.OpenSkyNetwork.RequestAPI;

import FlightTracker.Utilities.Parameter;

/**
 * AirportFlightsRequest supports requests for flights to/from a specified airport.
 *
 * Implementation of actual request (and other methods) are performed in parent classes:
 * @see FlightsRequest
 * @see BaseRequest
 *
 * NOTE: ***  THIS CLASS IS ABSTRACT AND CANNOT BE INSTANTIATED! ****
 *
 * @author Wade Hasbrouck - wadehas@gmail.com
 *
 */
public abstract class AirportFlightsRequest extends FlightsRequest{

    /**
     * Constructor for AirportFlightsRequest with OpenSkyNetwork User Credentials.
     *
     * @param operation - Request Operation.  i.e. "Arrivals" or "Departures"
     * @param airport   - Airport the request is for.  i.e. "KSEA"
     * @param begin     - Beginning of the time period for the request in epoch seconds
     * @param end       - End of the time period for the request in epoch seconds
     * @param userName  - OpenSkyNetwork username.  For anonymous user, set to empty string
     * @param password  - Password for specified user.  For anonymous user, set to empty string
     *
     * Request period should NOT exceed 7 days, or OpenSkyNetwork will return an error
     */
    public AirportFlightsRequest(String operation, String airport, long begin, long end, String userName, String password){
        super(operation, userName, password);
        this.parameters.add(new Parameter(Constants.AirportParamName, airport));
        this.parameters.add(new Parameter(Constants.BeginParamName, Long.toString(begin)));
        this.parameters.add(new Parameter(Constants.EndParamName, Long.toString(end)));
    }

    /**
     * Method for updateing the Parameters of the request to OpenSkyNetwork.  This is useful when using the same
     * instance for multiple requests to OpenSkyNetwork, i.e. for different time periods.
     *
     * @param airport   - Airport the request is for.  i.e. "KSEA"
     * @param begin     - Beginning of the request time period in epoch seconds
     * @param end       - End of the request time period in epoch seconds
     *
     * Request time period should NOT exceed 7 days, otherwise OpenSkyNetwork will return an error
     */
    public void UpdateParameters (String airport, long begin, long end){
        for (Parameter param : parameters){

            String name = param.getName();
            if (name == Constants.AirportParamName){
                param.setValue(airport);
            }
            else if (name == Constants.BeginParamName){
                param.setValue(Long.toString(begin));
            }
            else if(name == Constants.EndParamName){
                param.setValue(Long.toString(end));
            }
        }
    }
}
