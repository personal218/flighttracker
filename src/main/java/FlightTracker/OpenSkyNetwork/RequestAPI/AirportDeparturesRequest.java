package FlightTracker.OpenSkyNetwork.RequestAPI;

import java.time.LocalDateTime;

import static FlightTracker.Utilities.TimeUtilities.LocalDateTimeToEpochSeconds;

/**
 * AirportDeparturesRequest Class implements requests to OpenSkyNetwork to get departures from specified airport
 * for a specified time period.  Time period of the request should NOT exceed 7 days, otherwise OpenSkyNetwork will
 * return an error
 *
 * User Credentials are NOT required and do not provide any benefit over anonymous user.
 *
 * Actual request execution (and other methods) are implemented in parent classes:
 * @see BaseRequest
 * @see FlightsRequest
 * @see AirportFlightsRequest
 *
 * @author Wade Hasbrouck - wadehas@gmail.com
 */
public class AirportDeparturesRequest extends AirportFlightsRequest{

    /**
     * Constructor for AirportDeparturesRequest with OpenSkyNetwork credentials with begin and end in EpochSeconds
     *
     * @param airport   - Airport to request departures for.  e.g. "KSEA"
     * @param begin     - beginning of the request time period in epoch seconds
     * @param end       - end of the request time period in epoch seconds
     * @param userName  - OpenSkyNetwork username to use for the request
     * @param password  - Password for the specified user
     *
     * Request period should NOT exceed 7 days, otherwise OpenSkyNetwork will return an error.
     */
    public AirportDeparturesRequest(String airport, long begin, long end, String userName, String password){
        super(Constants.AirportDeparturesOperation, airport, begin, end, userName, password);
    }

    /**
     * Constructor for AirportDeparturesRequest as an Anonymous user with begin and end in EpochSeconds
     *
     * @param airport   - Airport to request departures for.  e.g. "KSEA"
     * @param begin     - beginning of the request period in epoch seconds
     * @param end       - end of the request period in epoch seconds
     *
     * Request period should NOT exceed 7 days, otherwise OpenSkyNetwork will return an error.
     */
    public AirportDeparturesRequest(String airport, long begin, long end){
        this(airport, begin, end, "","");
    }

    /**
     * Constructor for AirportDeparturesRequest with OpenSkyNetwork user credentials and Begin and End in LocalDateTime
     *
     * @param airport   - Airport to request departures for.  e.g. "KSEA"
     * @param begin     - Beginning of the request period as LocalDateTime
     * @param end       - End of the request period as LocalDateTime
     * @param username  - OpenSkyNetworkName to use for the request
     * @param password  - Password for the specified username
     *
     * Request period should NOT exceed 7 days, otherwise OpenSkyNetwork will return an error.
     */
    public AirportDeparturesRequest(String airport, LocalDateTime begin, LocalDateTime end, String username, String password){
        this(airport, LocalDateTimeToEpochSeconds(begin), LocalDateTimeToEpochSeconds(end), username, password);
    }

    /**
     * Constructor for AirportDeparturesRequest as an anonymous user with Begin and End as LocalDateTime
     * @param airport   - Aiport to request departures for.  e.g. "KSEA"
     * @param begin     - Beginning of the request period as LocalDateTime
     * @param end       - End of the request period as LocalDateTime
     *
     * Request period should NOT exceed 7 days, otherwise OpenSkyNetwork will return an error
     */
    public AirportDeparturesRequest(String airport, LocalDateTime begin, LocalDateTime end){
        this (airport, begin, end, "","");
    }
}
