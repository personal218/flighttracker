package FlightTracker.OpenSkyNetwork.RequestAPI;

import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

//TODO: This class should be either moved out of RequestAPI package to same package as Flight, State, and StateVectors classes, or those classes need to move her

/**
 * POJO class to contain information about a track for a flight.
 *
 * Instances of this class are populated by the JSON parsing in the TrackRequest class
 *
 * @author Wade Hasbrouck - wadehas@gmail.com
 * @version 1.0
 * @see TrackRequest
 */
public class FlightTrack {
    public String icao24;       // ICAO24 address of the aircraft
    public String callsign;     // Callsign of the aircraft that was used for the flight
    public long startTime;      // When the Flight started
    public long endTime;        // When the flight ended
    public List<Vector> path;   // list of State vectors showing path for the flight

    /**
     * Default constructor for FlightTrack.  Members are initialized to null, 0, or empty list as appropriate.
     */
    public FlightTrack(){
        this.icao24 = null;
        this.callsign = null;
        this.startTime = 0;
        this.endTime = 0;
        path = new LinkedList<>();
    }
}
