package FlightTracker.OpenSkyNetwork.RequestAPI;

import FlightTracker.Utilities.Parameter;
import java.time.LocalDateTime;

import static FlightTracker.Utilities.TimeUtilities.LocalDateTimeToEpochSeconds;

/**
 * This class implements a request for flights by a specific aircraft identified by its ICAO24 identifier.
 *
 * <p><b>
 *     Actual request execution (and other methods) are implemented in parent classes
 * </b></p>
 *
 * @see BaseRequest
 * @see FlightsRequest
 *
 * @author      Wade Hasbrouck - wadehas@gmail.com
 * @version     1.0
 * @since       1.0
 */
public class AircraftFlightRequest extends FlightsRequest{

    /**
     * Basic constructor for an Aircraft Flight Request with OpenSkyNetwork credentials.
     *
     * @param icao24    ICAO24 identifier of the aircraft. Address of the ADS-B transponder in the aircraft.
     * @param begin     beginning of the request period in epochSeconds
     * @param end       end of the request period in epochSceconds
     * @param userName  OpenSkyNetwork username for the request
     * @param password  Password for the OpenSkyNetwork user specified
     */
    public AircraftFlightRequest (String icao24, long begin, long end, String userName, String password){
        super(Constants.AircraftFlightsOperation, userName, password);
        this.parameters.add(new Parameter(Constants.Icao24ParamName, icao24));
        this.parameters.add(new Parameter(Constants.BeginParamName, Long.toString(begin)));
        this.parameters.add(new Parameter(Constants.EndParamName, Long.toString(end)));
    }

    /**
     * Basic constructor for an Aircraft Flight Request as an anonymous user
     *
     * @param icao24    ICAO24 identifier of the aircraft.  Address of the ADS-B transponder in the aircraft.
     * @param begin     beginning of the request period in epochSeconds
     * @param end       end of the request period in epochSeconds
     */
    public AircraftFlightRequest (String icao24, long begin, long end) {
        this (icao24, begin, end, "", "");
    }

    /**
     * Basic constructor for Aircraft Flight Request using LocalDateTime and OpenSkyNetwork Credentials
     *
     * @param icao24    ICAO24 identifier of the aircraft.  Address of the ADS-B transponder in the aircraft.
     * @param begin     Beginning of the request period as LocalDateTime
     * @param end       End of the request period in LocalDateTime
     * @param userName  OpenSkyNetwork Username to use for the reqeust
     * @param password  Password for the OpenSkyNetwork user that was specified
     */
    public AircraftFlightRequest(String icao24, LocalDateTime begin, LocalDateTime end, String userName, String password){
        this(icao24,
            LocalDateTimeToEpochSeconds(begin),
            LocalDateTimeToEpochSeconds(end),
            userName,
            password);
    }

    /**
     * Basic constructor for Aircraft Flight request as an anonymous user using LocalDateTime
     *
     * @param icao24    ICAO24 identifier of the aircraft.  Address of the ADS-B transponder in the aircraft.
     * @param begin     Beginning of the request period as LocalDateTime
     * @param end       End of the request period in LocalDateTime
     */
    public AircraftFlightRequest(String icao24, LocalDateTime begin, LocalDateTime end){
        this (icao24, begin, end, "", "");
    }

    /**
     * Method to update/change the ICAO24, begin, and end parameters of an Aircraft Flight Request
     *
     * @param icao24    ICAO24 identifier of the aircraft.  Address of the ADS-B transponder in the aircraft.
     * @param begin     Beginning of the request period as LocalDateTime
     * @param end       End of the request period in LocalDateTime
     */
    public void UpdateParameters (String icao24, long begin, long end){
        for (Parameter param : parameters){
            String name = param.getName();
            if (name == Constants.Icao24ParamName){
                param.setValue(icao24);
            }
            else if (name == Constants.BeginParamName){
                param.setValue(Long.toString(begin));
            }
            else if(name == Constants.EndParamName){
                param.setValue(Long.toString(end));
            }
        }
    }
}
