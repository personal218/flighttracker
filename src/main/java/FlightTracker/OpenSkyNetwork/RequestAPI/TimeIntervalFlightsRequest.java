package FlightTracker.OpenSkyNetwork.RequestAPI;

import FlightTracker.Utilities.Parameter;

import java.time.LocalDateTime;

import static FlightTracker.Utilities.TimeUtilities.LocalDateTimeToEpochSeconds;

/**
 * Class to support requests to OpenSkyNetwork for all flights during a specified time period
 *
 * @author  Wade Hasbrouck
 * @version 1.0
 * @since   1.0
 *
 * @see BaseRequest
 * @see FlightsRequest
 * @see <a href="https://opensky-network.org/apidoc/rest.html#flights-in-time-interval">OpenSkyNetwork Flights in Time Interval</a>
 */
public class TimeIntervalFlightsRequest extends FlightsRequest{
    /**
     * Constructor for TimeIntervalFlightRequest with OpenSkyNetwork credentials and Begin and End in epoch seconds.
     * <p>
     *     Specifying OpenSkyNetwork credentials does not give any additional benefit.
     * </p>
     * @param begin     Beginning of the time interval for the request in epoch seconds
     * @param end       End of the time interval for the request in epoch seconds
     * @param userName  OpenSkyNetwork username for the request.  For anonymous user, set to empty string
     * @param password  Password for specified username.  For anonymous user, set to empty string.
     */
    public TimeIntervalFlightsRequest (long begin, long end, String userName, String password){
        super (Constants.TimeIntervalFlightsOperation, userName, password);
        this.parameters.add(new Parameter(Constants.BeginParamName, Long.toString(begin)));
        this.parameters.add(new Parameter(Constants.EndParamName, Long.toString(end)));
    }

    /**
     * Constructor for TimeIntervalFlightRequest as anonymous user with Begin and End in epoch seconds.
     *
     * @param begin     Beginning of the time interval for the request in epoch seconds
     * @param end       End of the time interval for the request in epoch seconds
     */
    public TimeIntervalFlightsRequest(long begin, long end){
        this(begin, end, "", "");
    }

    /**
     * Constructor for TimeIntervalFlightRequest with OpenSkyNetwork credentials and Begin and End in LocalDateTime.
     * <p>
     *     Specifying OpenSkyNetwork credentials does not give any additional benefit.
     * </p>
     * @param begin     Beginning of the time interval for the request as LocalDateTime
     * @param end       End of the time interval for the request as LocalDateTime
     * @param userName  OpenSkyNetwork username for the request.  For anonymous user, set to empty string
     * @param password  Password for specified username.  For anonymous user, set to empty string.
     */
    public TimeIntervalFlightsRequest(LocalDateTime begin, LocalDateTime end, String userName, String password){
        this(LocalDateTimeToEpochSeconds(begin), LocalDateTimeToEpochSeconds(end), userName, password);
    }

    /**
     * Constructor for TimeIntervalFlightRequest with OpenSkyNetwork credentials and Begin and End in LocalDateTime.
     *
     * @param begin     Beginning of the time interval for the request as LocalDateTime
     * @param end       End of the time interval for the request as LocalDateTime
     */
    public TimeIntervalFlightsRequest(LocalDateTime begin, LocalDateTime end){
        this(begin, end, "", "");
    }

    /**
     * Method to update the paramaters for the request object to enable using the same object instance for multiple requests
     *
     * <p><b>NOTE: THIS CURRENTLY NOT IMPLEMENTED FOR THIS CLASS!</b></p>
     *
     * @param id    ID value for request.  either an airport, i.e. "KSEA" or icao24 address
     * @param begin start of time period for request in epoch seconds
     * @param end   end of time period for request in epoch seconds
     */
    //TODO: Need to implement this.
    public void UpdateParameters(String id, long begin, long end){
        throw new UnsupportedOperationException("Not Implemented:  This function is currently not implemented");
    }
}
