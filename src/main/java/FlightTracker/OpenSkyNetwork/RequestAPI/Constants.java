package FlightTracker.OpenSkyNetwork.RequestAPI;

/**
 * Class to contain Constants used for OpenSkyNetwork Request API.
 *
 * @author Wade Hasbrouck - wadehas@gmail.com
 *
 * THIS CLASS SHOULD ONLY CONTAIN CONSTANTS!
 */
public class Constants {
    // API operations
    public static final String AircraftFlightsOperation = "/flights/aircraft";
    public static final String AirportDeparturesOperation = "/flights/departure";
    public static final String AirportArrivalsOperation = "/flights/arrival";
    public static final String TimeIntervalFlightsOperation = "/flights/all";
    public static final String StateVectorsAllOperation = "/states/all";
    public static final String TracksOperation = "/tracks/all";

    // API Parameter names
    public static final String Icao24ParamName = "icao24";
    public static final String BeginParamName = "begin";
    public static final String EndParamName = "end";
    public static final String AirportParamName = "airport";
    public static final String TimeParamName = "time";
    public static final String MaxLatitudeParamName = "lamax";
    public static final String MaxLongitudeParamName = "lomax";
    public static final String MinLatitudeParamName = "lamin";
    public static final String MinLongitudeParamName = "lomin";

}
