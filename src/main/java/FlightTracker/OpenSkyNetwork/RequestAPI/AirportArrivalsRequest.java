package FlightTracker.OpenSkyNetwork.RequestAPI;

import java.time.LocalDateTime;

import static FlightTracker.Utilities.TimeUtilities.LocalDateTimeToEpochSeconds;

/**
 * Class: AirportArrivalsRequest
 *
 * This class is for requesting Arrivals at a specified airport during a specified time period.  The time period
 * should not exceed 7 days, otherwise OpenSkyNetwork will return an error.
 *
 * User credentials are not required and do not provide any extra benefit
 *
 * @author Wade Hasbrouck - wadehas@gmail.com
 */
public class AirportArrivalsRequest extends AirportFlightsRequest{

    /**
     * Constructor for AirportArrivalsRequest with OpenSkyNetwork Credentials.
     *
     * @param airport   - Airport to request arrivals for.  e.g. "KSEA"
     * @param begin     - Start point of the request period in Epoch Seconds
     * @param end       - End point of the request period in Epoch Seconds
     * @param userName  - Username for the request
     * @param password  - Password for the specified username
     *
     * Request period should not exceed 7 days, otherwise OpenSkyNetwork will return an error.
     */
    public AirportArrivalsRequest(String airport, long begin, long end, String userName, String password){
        super(Constants.AirportArrivalsOperation, airport, begin, end, userName, password);
    }

    /**
     * Contsructor for AirportArrivalsRequest as an anonymous user (no OpenSkyNetwork Credentials)
     *
     * @param airport   - Airport to request arrivals for.  e.g. "KSEA"
     * @param begin     - Start point of the request period in Epoch Seconds
     * @param end       - End point of the request period in Epoch Seconds
     *
     * Request period should not exceed 7 days, otherwise OpenSkyNetwork will return an error.
     */
    public AirportArrivalsRequest(String airport, long begin, long end){
        this(airport, begin, end, "", "");
    }

    /**
     * Constructor for AirportArrivalsRequest with OpenSkyNetwork Credentials using LocalDateTime for begin and end
     *
     * @param airport   - Airport to request arrivals for.  e.g. "KSEA"
     * @param begin     - Starting point of the request period as LocalDateTime
     * @param end       - Ending point of the request period as LocalDateTime
     * @param userName  - OpenSkyNetwork username for the request
     * @param password  - Password for the specified username
     *
     * Request period should not exceed 7 days, otherwise OpenSkyNetwork will return an error.
     */
    public AirportArrivalsRequest(String airport, LocalDateTime begin, LocalDateTime end, String userName, String password){
        this(airport, LocalDateTimeToEpochSeconds(begin), LocalDateTimeToEpochSeconds(end), userName, password);
    }

    /**
     * Constructor for AirportArrivalsRequest as an anonymous user using LocalDateTime for begin and end
     *
     * @param airport   - Airport to request arrivals for.  e.g. "KSEA"
     * @param begin     - starting point of the request period as LocalDateTime
     * @param end       - endping point of the request period as LocalDateTime
     *
     * Request period should not exceed 7 days, otherwise OpenSkyNetwork will return an error.
     */
    public AirportArrivalsRequest(String airport, LocalDateTime begin, LocalDateTime end){
        this(airport, begin, end, "", "");
    }
}
