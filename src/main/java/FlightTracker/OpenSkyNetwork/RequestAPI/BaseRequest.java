package FlightTracker.OpenSkyNetwork.RequestAPI;

import FlightTracker.Utilities.Parameter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import static FlightTracker.Utilities.ParameterStringBuilder.getParamsString;

/**
 * BaseRequest - Base class for all requests to OpenSkyNetwork API.  This class contains implementation for requests
 * as well as retrieving the responseCode and responseMessage.
 *
 * @author Wade Hasbrouck - wadehas@gmail.com
 */
public abstract class BaseRequest {
    // Constants
    private static final String domain  = "opensky-network.org";
    private static final String apiPath = "/api";

    // Protected Members
    private String operation = "";
    private String userName = "";
    private String password = "";
    private String responseMessage = "";
    private String responseContent = "";

    // Protected Members
    protected List<Parameter> parameters;

    /**
     * Default constructor for BaseRequest.  Initializes the parameters list
     */
    public BaseRequest(){
        this.parameters = new LinkedList<>();
    }

    /**
     * Constructor for BaseRequest specifying Operation and OpenSkyNetwork credentials
     *
     * @param operation - OpenSkyNetwork API operation to perform
     * @param userName  - OpenSkyNetwork username to use for the request
     * @param password  - password for the specified username
     */
    public BaseRequest(String operation, String userName, String password){
        this();
        this.operation = operation;
        this.userName = userName;
        this.password = password;
    }

    /**
     * Constructor for BaseReuest for specified operation as an anonymous user.
     *
     * @param operation - OpenSkyNetwork API operation to perform
     */
    public BaseRequest(String operation){
        this(operation, "", "");
    }

    /**
     * Method for executing the API Request to OpenSkyNetwork
     *
     * @return  int that is the returnCode from the API Request.  Standard HTTP return codes.  200 is "OK"
     * @throws IOException - Thrown if there is an issue with the httpConnection.
     */
    public int Execute() throws IOException {
        int returnValue = 0;
        String urlString = "https://";

        // if username is set, add username and password
        if (this.userName.length() > 0){
            urlString+= userName + ":" + password + "@";
        }

        urlString +=  domain + apiPath + operation;
        if (parameters.size() > 0){
            urlString += "?" + getParamsString(parameters);
        }

        // Build URL and httpConnection for the request
        URL url = new URL(urlString);
        HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
        httpConnection.setRequestMethod("GET");
        httpConnection.setRequestProperty("Content-Type", "application/json");

        // Execute the request (getResponseCode) and check value for success or not
        returnValue = httpConnection.getResponseCode();
        this.responseMessage = httpConnection.getResponseMessage();

        //TODO: Need to replace magic number with constant/enum
        if (returnValue == 200){ // succeeded - Store the returned content
            BufferedReader reader = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));

            String inputLine = "";
            responseContent = "";
            while ((inputLine = reader.readLine()) != null) {
                responseContent+= inputLine;
            }
        }

        // Disconnect and return
        httpConnection.disconnect();
        return returnValue;
    }

    /**
     * Method to get the responseMessage from the request.  This will contain a JSON formatted message.  If the request
     * returned an error, the message will indicate what the issue was.
     *
     * @return - responseMessage from the API request
     */
    public String getResponseMessage(){
        return this.responseMessage;
    }

    /**
     * Method that returns the responseContent from the API Request. This will contain the JSON objects from the
     * API Request of the request was successful.  If the request resulted in an error or empty result, this will
     * be set to empty string.
     *
     * @return - ResponseContent from the API Rquest
     */
    public String getResponseContent(){
        return this.responseContent;
    }
}
