package FlightTracker.OpenSkyNetwork.RequestAPI;

import FlightTracker.OpenSkyNetwork.Flight;
import FlightTracker.Utilities.TimeUtilities;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

/**
 * Abstract class to support requests for various Flight requests such as Arrivals, Departures, or specific aircraft
 *
 * @author Wade Hasbrouck - wadehas@gmail.com
 *
 * NOTE: THIS CLASS CANNOT BE INSTANTIATED!
 */
public abstract class FlightsRequest extends BaseRequest {

    /**
     * Constructor to create FlightsRequest with specified operation and OpenSkyNetwork credentials
     *
     * OpenSkyNetwork credentials are optional, and if not used set userName and password to empty string.
     * OpenSkyNetwork credentials do not provide any extra benefit.
     *
     * @param operation - OpenSkyNetwork operation to perform, i.e. "/api/arrivals"
     * @param userName  - OpenSkyNetwork username, set to empty string for anonymous user
     * @param password  - password for specified username, empty string for anonymous user
     */
    public FlightsRequest(String operation, String userName, String password){
        super(operation, userName, password);

    }

    /**
     * Parse JSON from responseContent to create a list of POJOs
     *
     * @return  - List of Flight objects from parsing the JSON in the responseContent
     * @throws JsonProcessingException - caused by error in JSON processing/parsing
     */
    public List<Flight> getFlights() throws JsonProcessingException {
        ObjectMapper jsonMapper = new ObjectMapper();
        String content = this.getResponseContent();
        List <Flight> flights = jsonMapper.readValue(content, new TypeReference<List<Flight>>(){});

        return flights;
    }

    /**
     * Output the Flight objects from the responseContent to the console.
     *
     * @throws JsonProcessingException - caused by error in JSON processing when calling getFlights()
     */
    public void OutputFlights() throws JsonProcessingException {
        for (Flight flight : this.getFlights()) {
            System.out.println(flight.callsign + "\t" +
                    TimeUtilities.EpochSecondsToDate(flight.firstSeen) + "\t" +
                    flight.estDepartureAirport + "-->" +
                    flight.estArrivalAirport + "\t" +
                    TimeUtilities.EpochSecondsToDate(flight.lastSeen));
        }
    }

    /**
     * Method to update request parameters so that the same object instance can be used for multiple requests
     *
     * @param id    - ID value for request.  either an airport, i.e. "KSEA" or icao24 address
     * @param begin - start of time period for request in epoch seconds
     * @param end   - end of time period for request in epoch seconds
     *
     * NOTE: THIS METHOD MUST BE IMPLEMENTED IN CHILD CLASSES
     */
    public abstract void UpdateParameters (String id, long begin, long end);
}
