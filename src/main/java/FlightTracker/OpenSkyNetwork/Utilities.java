package FlightTracker.OpenSkyNetwork;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.LinkedList;
import java.util.List;

public class Utilities {
    public static final String testStatesData = "{\"time\":1601245500,\"states\":[[\"a3e911\",\"FFT418  \",\"United States\",1601245499,1601245499,-104.1415,39.7222,4785.36,false,205.6,114.38,12.03,null,4884.42,null,false,0],[\"040190\",\"BOE106  \",\"Ethiopia\",1601245500,1601245500,-123.069,47.8422,5951.22,false,192.29,278.15,12.03,null,6286.5,\"4673\",false,0],[\"a1a949\",\"FSP602  \",\"United States\",1601245499,1601245499,-122.3015,47.837,1226.82,false,64.82,269.55,0.65,null,1348.74,\"4371\",false,0],[\"a1abec\",\"N2069M  \",\"United States\",1601245500,1601245500,-119.1148,34.2069,213.36,false,42.73,177.93,0.65,null,121.92,null,false,0],[\"aa56db\",\"UAL445  \",\"United States\",1601245476,1601245485,-81.7648,41.4685,746.76,false,90.31,230.78,-4.23,null,701.04,null,false,0],[\"7c6b2f\",\"JST828  \",\"Australia\",1601245499,1601245500,152.5801,-25.6282,10972.8,false,203.87,324.34,0,null,11292.84,null,false,0]]}";

    public static void ParseStatesJSON(String inputJSON) throws JsonProcessingException {
        inputJSON = testStatesData;
        //StateVectors stateVectors = new StateVectors();
        int index = 0;
        //List<String> jsonObjects = new LinkedList<>();
        String[] objects = inputJSON.split(":");

        ObjectMapper jsonMapper = new ObjectMapper();
        StateVectors stateVectors = jsonMapper.readValue(inputJSON, new TypeReference<StateVectors>() {});

        System.out.println("exiting parse...");

    }
}
