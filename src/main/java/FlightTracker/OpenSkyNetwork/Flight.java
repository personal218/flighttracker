package FlightTracker.OpenSkyNetwork;

public class Flight {
    public String icao24;
    public long firstSeen;
    public String estDepartureAirport;
    public long lastSeen;
    public String estArrivalAirport;
    public String callsign;
    public long estDepartureAirportHorizDistance;
    public long estDepartureAirportVertDistance;
    public long estArrivalAirportHorizDistance;
    public long estArrivalAirportVertDistance;
    public int departureAirportCandidatesCount;
    public int arrivalAirportCandidatesCount;

    public Flight(){
        this.icao24 = null;
        this.firstSeen = 0;
        this.estDepartureAirport = null;
        this.lastSeen = 0;
        this.estArrivalAirport = null;
        this.lastSeen = 0;
        this.callsign = null;
        this.estDepartureAirportHorizDistance = 0;
        this.estDepartureAirportVertDistance = 0;
        this.estArrivalAirportHorizDistance = 0;
        this.estArrivalAirportVertDistance = 0;
        this.departureAirportCandidatesCount = 0;
        this.arrivalAirportCandidatesCount = 0;
    }
}
