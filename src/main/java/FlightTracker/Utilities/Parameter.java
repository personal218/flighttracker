package FlightTracker.Utilities;

public class Parameter {
    private String name;
    private String value;

    public Parameter(String name, String value){
        this.name = name;
        this.value = value;
    }

    public Parameter(String name){
        this.name = name;
        this.value = "";
    }

    public void setValue (String value){
        this.value = value;
    }

    public String getName(){
        return this.name;
    }

    public String getValue(){
        return this.value;
    }
}

