package FlightTracker.Utilities;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;

public class TimeUtilities {

    // Convert LocalDateTime to Epochs
    public static long LocalDateTimeToEpochMillis (LocalDateTime localDateTime){
        long returnValue = localDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        return returnValue;
    }

    public static long LocalDateTimeToEpochSeconds(LocalDateTime localDateTime){
        long returnValue = LocalDateTimeToEpochMillis(localDateTime) / 1000; // divide by 1000 too get seconds
        return returnValue;
    }

    // Versions that call with LocalDateTime.now() for convenience.
    public static long LocalDateTimeToEpochMillis(){
        return LocalDateTimeToEpochMillis(LocalDateTime.now());
    }

    public static long LocalDateTimeToEpochSeconds(){
        return LocalDateTimeToEpochSeconds(LocalDateTime.now());
    }

    // Covert Epochs to LocalDateTime
    public static LocalDateTime EpochSecondsToDate(long epochSeconds){
        LocalDateTime localDateTime = Instant.ofEpochSecond(epochSeconds)
                                             .atZone(ZoneId.systemDefault())
                                             .toLocalDateTime();
        return localDateTime;
    }
}
