package FlightTracker.Utilities;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

public class ParameterStringBuilder {
    public static String getParamsString(Map<String, String> params, String encoding)
            throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();

        for (Map.Entry<String, String> entry : params.entrySet()) {
            result.append(URLEncoder.encode(entry.getKey(), encoding));
            result.append(URLEncoder.encode("=", encoding));
            result.append(URLEncoder.encode(entry.getValue(), encoding));
            result.append(URLEncoder.encode("&", encoding));
        }

        String resultString = result.toString();
        return resultString.length() > 0
                ? resultString.substring(0, resultString.length() - 1)
                : resultString;
    }

    public static String getParamString(Map<String, String> params) throws UnsupportedEncodingException {
        return getParamsString(params, "UTF-8");
    }

    public static String getParamsString(List<Parameter> parameterList, String encoding) throws UnsupportedEncodingException {
        String returnValue = "";

        StringBuilder result = new StringBuilder();

        for (Parameter parameter : parameterList){
            result.append(parameter.getName());
            result.append("=");
            result.append(parameter.getValue());
            result.append("&");
        }

        returnValue = result.toString();
        return returnValue.length() > 0
                ? returnValue.substring(0, returnValue.length() - 1)
                : returnValue;

    }

    public static String getParamsString(List<Parameter> parameterList) throws UnsupportedEncodingException {
        return getParamsString(parameterList, "UTF-8");
    }
}
